#!/bin/bash
echo "Altualna wersja wymaga uruchomienia jako root!"

echo "--------------------"
echo "Przygotowanie do instalacji certyfikatow..."
echo "--------------------"
printf 'DOCKER_OPTS="--insecure-registry whiteasterhub.zyns.com:5000"' >> /etc/default/docker
touch /etc/docker/daemon.json
printf '{\n     "insecure-registries": ["whiteasterhub.zyns.com:5000"]\n}' >> /etc/docker/daemon.json
service docker restart

echo "--------------------"
echo "Pobieranie certyfikatow..."
echo "--------------------"
mkdir /tmp/certs
# Do zmiany scp, testowo na resources, trzeba wystawić jakiś serwer.
scp dockerhub@192.168.1.59:/home/dockerhub/Docker/registry/certs/domain.* /tmp/certs 

echo "--------------------"
echo "Instalowanie certyfikatow..."
echo "--------------------"
mkdir -p /etc/docker/certs.d/whiteasterhub.zyns.com:5000
cp /tmp/certs/domain.* /etc/docker/certs.d/whiteasterhub.zyns.com:5000
service docker restart