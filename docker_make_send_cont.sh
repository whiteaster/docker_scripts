#!/bin/bash

echo "--------------------"
echo "Tworzenie kontenera..."
echo "--------------------"

#Inicjalizacja zmiennych
hname=$(hostname)
act_time=$(date +"%T")

# Nazwa kontenera generowana jest w formacie: nazwa buildu_hostname komputera_aktualny czas 
docker build -t whiteasterhub.zyns.com:5000/solr solr/7.3/.
docker build -t whiteasterhub.zyns.com:5000/frontend_$hname\_$act_time frontend/.
docker build -t whiteasterhub.zyns.com:5000/backend_$hname\_$act_time backend/.

echo "--------------------"
echo "Pushowanie kontenera..."
echo "--------------------"
docker push whiteasterhub.zyns.com:5000/solr
docker push whiteasterhub.zyns.com:5000/frontend_$hname\_$act_time
docker push whiteasterhub.zyns.com:5000/backend_$hname\_$act_time
